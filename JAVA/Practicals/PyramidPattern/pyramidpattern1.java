 import java.util.*;
	class one{
		public static void main(String[] args){
			Scanner sc = new Scanner(System.in);
			System.out.print("Enter rows: ");
			int row= sc.nextInt();
			for(int i=1; i<=row; i++){
				for(int space=1; space<i; space++){
					System.out.print(" " + "\t");
				}
				int num=1;
				for(int j=1; j<=i; j++){
					System.out.print(num + " ");
				}
				System.out.println();
			}
		}
	}
